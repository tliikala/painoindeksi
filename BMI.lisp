;;; -*- Mode: Common-Lisp; Author: Teemu Liikala -*-

;;; Painoindeksi-sovellus 
;;; This is just an example.

(in-package "CL-USER") ;;; oletus

;;; Suorittaminen: (COMMON-LISP-USER::aja-bmi) tai (aja-bmi) oltaessa paketissa COMMON-LISP-USER

(defclass hl� ()
  ((paino :initform 0 :initarg :paino :reader lue-paino :documentation "[kg]")
   (pituus :initform 0 :initarg :pituus :reader lue-pituus :documentation "[m]")
   (painoindeksi :accessor aksessori-painoindeksi :documentation "painoindeksin arvo asetetaan t�h�n"))
  )

(defmethod laske-painoindeksi ((self hl�))
  (with-slots (paino pituus painoindeksi) self
    (setq painoindeksi (/ paino (* pituus pituus)))))

#|
https://fi.wikipedia.org/wiki/Painoindeksi

alle 15	        sairaalloinen alipaino
15  � 17	merkitt�v� alipaino
17  � 18,5	normaalia alhaisempi paino
18,5 � 25	normaali paino
25  � 30	liev� ylipaino
30  � 35	merkitt�v� ylipaino
35  � 40	vaikea ylipaino
yli 40	        sairaalloinen ylipaino
|#

;;; Defgeneric ei olisi t�ss� tapauksessa v�ltt�m�t�n. Se on vain ":documentation" varten.
(defgeneric selit�-painoindeksi (object)
  (:documentation "antaa sanallisen kuvauksen painoindeksist� (painoindeksi pit�� olla saatavilla, eli esim. 'laske-painoindeksi kutsuttu ainakin kerran)"))

(defmethod selit�-painoindeksi ((self hl�))
  (with-slots (painoindeksi) self
    (when (numberp painoindeksi)
      ;;; Huomaa, ett� kuvaukset ovat ep�m��r�isi� rajoilla!
      (cond ((< painoindeksi 15) "sairaalloinen alipaino")
            ((and (>= painoindeksi 15) (<= painoindeksi 17)) "merkitt�v� alipaino")
            ((and (> painoindeksi 17) (<= painoindeksi 18.5)) "normaalia alhaisempi paino")
            ((and (> painoindeksi 18.5) (<= painoindeksi 25)) "normaali paino")
            ((and (> painoindeksi 25) (<= painoindeksi 30)) "liev� ylipaino")
            ((and (> painoindeksi 30) (<= painoindeksi 35)) "merkitt�v� ylipaino")
            ((and (> painoindeksi 35) (<= painoindeksi 40)) "vaikea ylipaino")
            ((> painoindeksi 40) "sairaalloinen ylipaino")
            (T "jokin virhe..?")))))

;;; Listener: (perustesti)
(defun perustesti ()
  (let ((hl� (make-instance 'hl� :paino 80 :pituus 1.8)))
    (values (laske-painoindeksi hl�) (selit�-painoindeksi hl�))))

;;;
;;; GUI:n osuus
;;; 

(capi:define-interface laske-painoindeksi-gui ()
  ((hl� :initform nil :accessor kirj-hl� :documentation "vain k�ytt�liittym�� varten"))
  (:panes
   (pituus-txt
    capi:text-input-pane
    :title "Pituus [m]"
    :change-callback-type :interface
    :text-change-callback 'sopiva-sy�te)
   (paino-txt
    capi:text-input-pane
    :title "Paino [kg]"
    :change-callback-type :interface
    :text-change-callback 'sopiva-sy�te)
   (laske
    ;;; Nappi on aktiivinen vain, jos pituus ja paino ovat "ok".
    capi:push-button
    :text "Laske painoindeksi"
    :enabled nil
    :callback #'(lambda (&rest ignore)
                  (declare (ignore ignore))
                  ;;;
                  (with-slots (paino pituus) hl�
                    (setq paino (hcl:parse-float (capi:text-input-pane-text paino-txt))
                          pituus (hcl:parse-float (capi:text-input-pane-text pituus-txt))))
                  ;;;
                  (laske-painoindeksi hl�) ;;; tarvitsee tehd�
                  ;;;
                  (setf (capi:title-pane-text bmi-kuvaus)
                        (format nil "Painoindeksi: ~D (~A)" (aksessori-painoindeksi hl�) (selit�-painoindeksi hl�)))))
   (bmi-kuvaus
    capi:title-pane
    :accessor aksessori-bmi-kuvaus
    :text "   "))
  (:layouts
   (p��-layout
    capi:column-layout
    '(pituus-txt paino-txt bmi-kuvaus laske)))
  (:default-initargs
   :layout 'p��-layout :auto-menus nil
   :title "Painoindeksi"
   :best-height 200 :best-width 400))

(defmethod initialize-instance :after ((self laske-painoindeksi-gui) &key)
  (setf (kirj-hl� self) (make-instance 'hl�)))

(defmethod sopiva-sy�te ((self laske-painoindeksi-gui))
  ;;; Kutsutaan vain "callback":ss�, joten tiedet��n, ett� suoritetaan k�ytt�liittym�n "s�ikeess�"!
  (flet ((sy�te-ok-p (sy�te-txt)
           (lw:when-let (sy�te (ignore-errors (hcl:parse-float sy�te-txt)))
             (plusp sy�te) ;;; vaaditaan positiivinen numeroarvo
             )))
    (with-slots (pituus-txt paino-txt laske bmi-kuvaus) self
      (setf (capi:title-pane-text bmi-kuvaus) "   " ;;; tyhjennet��n kentt�
            ;;; Huomaa, ett� yksi setf voi sis�lt�� useita asetettavia asioita.
            (capi:button-enabled laske)
            (and (sy�te-ok-p (capi:text-input-pane-text pituus-txt))
                 (sy�te-ok-p (capi:text-input-pane-text paino-txt)))
            )
      ;;; (capi:redisplay-interface self) ;;; Joskus muutokset vaativat k�ytt�liittym�n uudelleen n�ytt�misen.
      )))

;;; Listener: (aja-bmi)
(defun aja-bmi ()
  (capi:display (make-instance 'laske-painoindeksi-gui)))
