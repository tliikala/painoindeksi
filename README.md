Tämä repositorio sisältää yksinkertaisen ihmisen painoindeksin laskevan esimerkkisovelluksen.
Sovellus on toteutettu Common Lisp -ohjelmointikielellä.
Sovellus on tarkoitettu esimerkiksi sekä Common Lisp -ohjelmoinnin harjoittelun tueksi. Käyttö omalla vastuulla.
laatija: Teemu Liikala, 2017

työkalu: LispWorks Personal Edition 6.1.1 (for MS Windows)

OHJEET
Avaa tiedosto BMI.lisp LispWorks:in editorissa.
Editorissa valitse: File ---> Compile and Load.
Siirry Listener-työkaluun ja syötä: (COMMON-LISP-USER::aja-bmi).
Painoindeksi-sovelluksen käyttöliittymän tulisi avautua.